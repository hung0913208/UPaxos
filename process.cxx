#include "playground.hpp"
#include "message.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

/* mainboard, nơi thiết kế cách hoạt động chính của chương trình */

/* hàm này có trách nhiệm đọc các token từ 1 file stream (fstream) lên và nạp vào vector
   số token cần đọc được quy định bằng count
 */

typedef struct sockaddr_in sockaddr_in;

struct ClientReq {
	sockaddr_in address;
	char* 		ptr;
	size_t 		size;

	explicit ClientReq(MCmdMsg& cmd): ptr{nullptr} { 
		if (cmd.ptr) {
			size    = getLoWord(cmd.ptr, 0) - sizeof(Word) - sizeof(sockaddr_in) - 1;
			address = getObject(cmd.ptr, sizeof(Word), sockaddr_in);
			ptr 	= getRaw(cmd.ptr, sizeof(Word) + sizeof(sockaddr_in));
		}
	}
};

extern "C" {
	enum Operation {
		GET = 0,
		SET = 1
	};

	typedef struct CCommand {
		struct timespec ts;
		enum Operation op;
		Word cliId, thrId;
		DWord cmdId;
		char dt[32];
	} CCommand;
}

struct Command {
	CCommand detail;
	
#define COMMAND_ID_OFFSET 0
#define CLIENT_ID_OFFSET sizeof(DWord)
#define THREAD_ID_OFFSET sizeof(DWord) + sizeof(Word)
#define OPERATION_OFFSET sizeof(DWord) + 2*sizeof(Word)
#define TIMESPEC_OFFSET sizeof(DWord) + 2*sizeof(Word) +sizeof(enum Operation)
#define CONTENT_OFFSET sizeof(DWord) + 2*sizeof(Word) +sizeof(enum Operation) + sizeof(timespec)

	explicit Command(ClientReq& request) {
		if (request.ptr) {
			detail.cmdId = getLoDWord(request.ptr, COMMAND_ID_OFFSET);
			detail.cliId = getLoWord(request.ptr,  CLIENT_ID_OFFSET);
			detail.thrId = getLoWord(request.ptr, THREAD_ID_OFFSET);
			detail.op = getObject(request.ptr, OPERATION_OFFSET, enum Operation);
			detail.ts = getObject(request.ptr, TIMESPEC_OFFSET, timespec);
			memcpy(detail.dt, request.ptr + CONTENT_OFFSET, 32);
		}
	}
};

void deliver(int sockid, upaxos::Configure<Caans, TLearner>& config, MCmdMsg &msg, TLearner& learner) {
	ClientReq request{msg};
	printf("prepare deliver\n");

	if (config.leveldb) {
		Command cmd{request};

		switch (cmd.detail.op) {
		case GET:
			if (add_entry(learner.leveldb, 0, cmd.detail.dt, 16, &cmd.detail.dt[16], 16))
				fprintf(stderr, "Add entry failed.\n");
			break;

		case SET: {
			char* stored_value = nullptr;
			size_t vsize = 0;

			if (get_value(learner.leveldb, cmd.detail.dt, 16, &stored_value, &vsize))
				fprintf(stderr, "get value failed.\n");
			else if (stored_value) {
				printf("Stored value %s, size %zu", stored_value, vsize);
				free(stored_value);
			}
			break;
		}
		}
	}

	printf("prepare repsonse, learner id %ld\n", config.iddentity);
	if (config.iddentity == 0) {
		print_addr(&request.address);
		int n = sendto(sockid, request.ptr, request.size, 0,
		               (struct sockaddr*)&request.address,
		               sizeof(request.address));
		if (n < 0) perror("deliver: sendto error");
		printf("already repsonse\n");
	}

}

void print_addr(struct sockaddr_in* s) {
	printf("address %s, port %d\n", inet_ntoa(s->sin_addr), ntohs(s->sin_port));
}

bool load(std::fstream& cfg, std::vector<std::string>& output, std::size_t count) {
	std::string value;

	for (; count > 0 && !cfg.eof(); --count) {
		cfg >> value;
		output.push_back(value);
	}
	return count == 0;
}

template<typename T>
void send(int sockid, PaxosType type, T msg, std::vector<Caans::Address>& acceptors) {
	char 		header[128];
	sockaddr_in addr;

	for (auto& acceptor : acceptors) {
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = acceptor.address();
		addr.sin_port = htons(acceptor.port());

		memcpy(header, &type, sizeof(Word));
		memcpy(header + sizeof(Word), &msg, sizeof(T));
		sendto(sockid, header,
		       sizeof(Word) + sizeof(T), 0,
		       (struct sockaddr*)&addr, sizeof(sockaddr_in));
	}
}

int deserilization(char* ptr, int size, Protocol & msg) {
	ptr[size] = '\0';
	return getThreadId(msg = ptr);
};

/* check holes sau một chu kì hoặc khi số lượng wait cao hơn một mức xác định */
void vanquish(int sockid, upaxos::Configure<Caans, TLearner>& config, std::size_t thread, Process & content) {
	auto to   = content.machine.hiwait();
	auto from = content.machine.running();

	if (to > from) {
		for (auto iid = from; iid < to; ++iid) {
			send(sockid, PaxosPrepare,
			     MLearnerMsg{DWord(iid), 3, Word(thread)},
			     config.content().acceptors);
		}
	}
}

/* hàm này chịu trách nhiệm phân tích và xử lý data ngay tại chỗ luôn */
upaxos::Error handle(int sockid, upaxos::Configure<Caans, TLearner>& config, Executor* exec, char*& msg, int size) {
	try {
		if (Cast(exec)->content().machine.track() > config.content().track)
			vanquish(sockid, config, exec->id, Cast(exec)->content());

		printf("** x\n");
		if (msg && size > 0) {
			switch (getType(msg)) {
			case PaxosPrepare:
				break;

			case PaxosPromise:
				break;

			case PaxosAccepted: {
				printf("** accepted\n");
				MLearnerMsg accepted = getAcceptedMsg(msg, size);
				MCmdMsg 	cmd;
				paxos::IId 	iid;
				printf("iid %u ballot %u t_id %u atid %u  aid %u\n",
				       accepted.iid,  accepted.ballot, accepted.thid, accepted.atid, accepted.aid);
				Cast(exec)->content().machine.load(std::make_tuple(accepted.iid, accepted.aid),
				                                   accepted.ballot,
				                                   accepted.cmd);

				while ((iid = Cast(exec)->content().machine.deliver(cmd)) != std::string::npos)
				{
					printf("while iid %ld\n", iid);
					deliver(sockid, config, cmd, Cast(exec)->content());
				}
				break;
			}

			default:
				break;
			}
		}
		return NoError;
	} catch (upaxos::Error& error) { return error;}
}

bool compare(MCmdMsg& a, MCmdMsg& b) {
	if (a.size != b.size) return false;
	else if (a.ptr == b.ptr) return true;
	else return memcmp(a.ptr, b.ptr, a.size);
}
