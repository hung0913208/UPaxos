#define _GNU_SOURCE
#include <sys/socket.h>

#include "background.h"

#define MMSGHDR struct mmsghdr
#define IOVEC   struct iovec
void* handleReceiver(void* parameter) {
	Receiver* receiver      = (Receiver*) parameter;
	Controller controller  = receiver->controller;
	int retval              = 0, i = 0;

	MMSGHDR* messages = receiver->cache.messages;
	IOVEC*   iovecs   = receiver->cache.iovecs;
	char**   buffer   = receiver->cache.buffer;

	if (messages && iovecs && buffer) {
		for (i = 0; i < controller.max.msg_buffer; i++) {
			MMSGHDR* msg = &messages[i];
			IOVEC* iovec = &iovecs[i];

			msg->msg_hdr.msg_iov = iovec;
			msg->msg_hdr.msg_iovlen = 1;

			iovec->iov_base = buffer[i];
			iovec->iov_len  = controller.max.msg_length;
		}
		if (controller.parser == NULL) return NULL;

		while ((retval = recvmmsg(receiver->sockid, messages, controller.max.msg_buffer, MSG_WAITFORONE, NULL)) > 0) {
			for (i = 0; i < retval; ++i) {
				Message msg = controller.parser(receiver, buffer[i], messages[i].msg_len);

				if (0 <= msg.id && msg.id < controller.count) {
					msg.contract.sockid = receiver->sockid;
#if INSTALL_GARBAGE
					requireTask(&controller.executors[msg.id], msg.contract, msg.contract.msg != buffer[retval - 1]);
#else
					requireTask(&controller.executors[msg.id], msg.contract);
#endif
				}
#if DEBUG

#endif
			}
		}
	}
	return NULL;
}
