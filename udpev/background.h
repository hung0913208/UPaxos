#ifndef TASK_H
#define TASK_H
#include "udpev.h"
#if __cplusplus
extern "C" {
#endif

#if INSTALL_GARBAGE
	void  requireTask(Executor* executor, Contract contract, int allocated);
#else
	void  requireTask(Executor* executor, Contract contract);
#endif
	void  chooseNextTask(Executor* executor, int step);

	void* entry2Executor(void* parameter);
	void* handleReceiver(void* parameter);
#if __cplusplus
}
#endif
#endif