#ifndef UDPEV_H 
#define UDPEV_H
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "definition.hpp"

#if __cplusplus
extern "C" {
#endif

#define DEBUG 		1

/* basic structures */

#ifndef Protocol
#define DEFAULT_PROTOCOL	void*
#define INSTALL_GARBAGE   	1

#define Protocol DEFAULT_PROTOCOL
#endif

#define DEFAULT_MAX_MESSAGE 			10
#define DEFAULT_MAX_LENGTH_OF_MESSAGE   100

struct Receiver;
struct Executor;
struct Contract;
struct Message;

typedef struct Contract {
	int 	 sockid, size;
	Protocol msg;
} Contract;

typedef struct Message {
	Contract contract;
	int 	 id;
} Message;

/* callback definitions */
typedef int (*Runner)(struct Executor*, struct Contract*);
typedef struct Message (*Parser)(struct Receiver*, char*, int);

typedef struct Schedule {
	int        	begin, end, max;
	Contract*	requirements;
#if INSTALL_GARBAGE
	int* 		allocated;
#endif
} Schedule;

typedef struct Executor {
	pthread_mutex_t waiting, updating;
	Schedule        scheduler;
	pthread_t       thread;
	Runner          callback;
	void*           content;
	int             id;
} Executor;

typedef struct Controller {
	Executor*   executors;
	Parser      parser;
	int         count, numOfPacket;

	struct Max {
		int msg_buffer, msg_length;
	} max;
} Controller;

#define MMSGHDR struct mmsghdr
#define IOVEC   struct iovec
typedef struct Receiver {
	struct Cache{
		MMSGHDR* messages;
		IOVEC*   iovecs;
		char**   buffer;
	} cache;
	Controller  controller;
	pthread_t   thread;
	int         sockid, port, id_receiver;
} Receiver;

/* interface functions */
Controller* installExecutors(Runner* runners, int count, Parser parser, int bufsize);
int 	    installReceivers(Receiver* receivers, int count, in_addr_t owner, int port, Controller* controller);
void 	    startService(Receiver* receivers, int creceivers, Controller* controller, int ccontroler);
void 	    releaseController(Controller* controller, int count);
#if __cplusplus
}
#endif
#endif
