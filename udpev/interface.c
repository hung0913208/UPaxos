#define _GNU_SOURCE

#include <netinet/ip.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <assert.h>

#include "background.h"

void startService(Receiver* receivers, int creceivers, Controller* controller, int ccontroler) {
    int  i = 0, j = 0;
    for (i = 0; i < creceivers; ++i)
        pthread_join(receivers[i].thread, NULL);
    for (i = 0; i < ccontroler; ++i)
        for (j = 0; j < controller->count; ++j)
            pthread_join(controller->executors[j].thread, NULL);
}

Controller* installExecutors(Runner* runners, int count, Parser parser, int bufsize) {
    Controller* result = (Controller*)malloc(sizeof(Controller));
    int i = 0;

    if (runners == NULL || result == NULL || count <= 0 || parser == NULL) {
        if (result) free(result);
        return NULL;
    } else memset(result, 0, sizeof(Controller));
    if (!(result->executors   = (Executor*)malloc(count * sizeof(Executor)))) {
        free(result);
        return NULL;
    } else memset(result->executors, 0, count * sizeof(Executor));

    result->count       = count;
    result->parser      = parser;
    result->numOfPacket = 0;

    result->max.msg_length = DEFAULT_MAX_LENGTH_OF_MESSAGE;
    result->max.msg_buffer = DEFAULT_MAX_MESSAGE;

    for (i = 0; i < count; ++i) {
        int error = 0;

        result->executors[i].id = i;
        result->executors[i].callback = runners[i];
        result->executors[i].scheduler.requirements = (Contract*)malloc(bufsize * sizeof(Contract));
#if INSTALL_GARBAGE
        result->executors[i].scheduler.allocated    = (int*)malloc(bufsize * sizeof(int));
#endif
        result->executors[i].scheduler.max   = bufsize;
        result->executors[i].scheduler.begin = 0;
        result->executors[i].scheduler.end   = 0;

        pthread_mutex_init(&result->executors[i].waiting, NULL);
        pthread_mutex_init(&result->executors[i].updating, NULL);

        if (!result->executors[i].scheduler.requirements) {
            releaseController(result, count);
        } else if ((error = pthread_create(&result->executors[i].thread, NULL, entry2Executor, &result->executors[i])) != 0) {
            printf("\ncan't create thread %d :[%s]", i, strerror(error));
            releaseController(result, count);
        }
    }
    return result;
}

int isMulticastIp(in_addr_t addr) {
    return (addr & 0xFF) >= 224 && (addr & 0xFF) <= 239;
}

#define SOCKADDR_IN struct sockaddr_in
int installReceivers(Receiver* receivers, int count, in_addr_t owner, int port, Controller* controller) {
    int         yes = 1, idx, i;
    SOCKADDR_IN addr;

    if (controller == NULL || receivers == NULL)
        return -1;
    else {
        memset(&addr, 0, sizeof(struct sockaddr_in));
        addr.sin_family         = AF_INET;
        addr.sin_port           = htons(port);
        addr.sin_addr.s_addr    = owner;
    }

    for (idx = 0; idx < count; ++idx) {
        receivers[idx].controller = *controller;
        receivers[idx].port       = port;

        receivers[idx].cache.messages = (MMSGHDR*)calloc(controller->max.msg_buffer, sizeof(MMSGHDR));
        receivers[idx].cache.iovecs   = (IOVEC*)  calloc(controller->max.msg_buffer, sizeof(IOVEC));
        receivers[idx].cache.buffer   = (char**)  calloc(controller->max.msg_buffer, sizeof(char*));

        if (receivers[idx].cache.messages && receivers[idx].cache.iovecs && receivers[idx].cache.buffer) {
            for (i = 0; i < controller->max.msg_buffer; ++i) {
                receivers[idx].cache.buffer[i] = (char*) calloc(controller->max.msg_length, sizeof(char));
                if (!receivers[idx].cache.buffer[i]) goto _clean;
            }

            if ((receivers[idx].sockid = socket(AF_INET, SOCK_DGRAM, 0)) >= 0) {
                if (owner != INADDR_ANY && isMulticastIp(owner)) {
                    struct ip_mreq mreq;

                    mreq.imr_multiaddr.s_addr = owner;
                    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
                    if (setsockopt(receivers[idx].sockid, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
                        perror("IP_ADD_MEMBERSHIP");
                        goto _release;
                    }
                }

                if (setsockopt(receivers[idx].sockid, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) >= 0)
                    if (setsockopt(receivers[idx].sockid, SOL_SOCKET, SO_REUSEPORT, (char*)&yes, sizeof(yes)) >= 0) {
                        if (bind(receivers[idx].sockid, (struct sockaddr*)(&addr), sizeof(addr)) >= 0) {
                            int error = pthread_create(&receivers[idx].thread, NULL, handleReceiver, &receivers[idx]);

                            if (error == 0) continue;
                            else printf("\ncan't create thread %d :[%s]", idx, strerror(error));
                        }
                    }
            }
_release:
            for (i = 0; i < idx; ++i) {
                if ((int)controller->executors[i].thread < 0) continue;
                pthread_cancel(controller->executors[i].thread);
            }
            releaseController(controller, controller->count);
            return -1;
_clean:
            for (i = 0; receivers[idx].cache.buffer[i]; ++i)
                free(receivers[idx].cache.buffer[i]);
            if (receivers[idx].cache.messages)
                free(receivers[idx].cache.messages);
            if (receivers[idx].cache.iovecs)
                free(receivers[idx].cache.iovecs);
            if (receivers[idx].cache.buffer)
                free(receivers[idx].cache.buffer);
        }
    }

    return 0;
}

void releaseController(Controller* controller, int count) {
    for (count--; count >= 0; --count) {
        if ((int)controller->executors[count].thread < 0) continue;
        pthread_cancel(controller->executors[count].thread);
    }

    free(controller->executors);
    free(controller);
}