#include "background.h"
#include <stdlib.h>

#if DEBUG
#include <stdio.h>
#endif

void chooseNextTask(Executor* executor, int step) {
#if DEBUG
	printf("chooseNextTask: begin %d, end %d\n", executor->scheduler.begin, executor->scheduler.end);
#endif
	if (step == 0) return;
	else if (executor->scheduler.begin % executor->scheduler.max != (executor->scheduler.end + 1) % executor->scheduler.max) {
#if DEBUG
		printf("chooseNextTask: lock updating\n");
#endif
		for (; step > 0; --step) {
			if ((executor->scheduler.begin + 1) % executor->scheduler.max == executor->scheduler.end % executor->scheduler.max) {
#if INSTALL_GARBAGE
				if (executor->scheduler.allocated[executor->scheduler.begin]){
					if (executor->scheduler.requirements[executor->scheduler.begin].msg != NULL)
						free(executor->scheduler.requirements[executor->scheduler.begin].msg);
				}
#endif
#if DEBUG
				printf("entry2Executor: unlock updating\n");
#endif
				pthread_mutex_unlock(&executor->updating);
			}

			if (executor->scheduler.begin + 1 < executor->scheduler.max)
				executor->scheduler.begin++;
			else executor->scheduler.begin = 0;
		}
#if DEBUG
		printf("chooseNextTask: unlock updating\n");
#endif
	} else if (executor->scheduler.begin + step < executor->scheduler.max)
		executor->scheduler.begin += step;
	else executor->scheduler.begin = step - executor->scheduler.max + executor->scheduler.begin;
}

void* entry2Executor(void* parameter) {
	Executor* executor = (Executor*) parameter;
	int step = 0;

	for (;;) {
		if (pthread_mutex_trylock(&executor->waiting)) {
#if DEBUG
			printf("entry2Executor: lock waiting\n");
#endif
			pthread_mutex_lock(&executor->waiting);
		}
		while (executor->scheduler.begin != executor->scheduler.end % executor->scheduler.max) {
#if DEBUG
			printf("entry2Executor: begin calculating\n");
#endif
			step = executor->callback(executor, &executor->scheduler.requirements[executor->scheduler.begin]);
			if (step >= 0) chooseNextTask(executor, step);
#if DEBUG
			printf("entry2Executor: end calculating\n");
#endif
			if ((executor->scheduler.begin + 1) % executor->scheduler.max == executor->scheduler.end % executor->scheduler.max) {
#if DEBUG
				printf("entry2Executor: unlock updating\n");
#endif
				pthread_mutex_unlock(&executor->updating);
			}
		}
	}
	return NULL;
}

void requireTask
#if INSTALL_GARBAGE
(Executor* executor, Contract contract, int allocated)
#else
(Executor* executor, Contract contract)
#endif
{
	if (pthread_mutex_trylock(&executor->updating)) {
#if DEBUG
		printf("requireTask: lock updating\n");
#endif
		pthread_mutex_lock(&executor->updating);
	}
#if DEBUG
	printf("requireTask: begin %d, end %d\n", executor->scheduler.begin, executor->scheduler.end);
#endif
	executor->scheduler.requirements[executor->scheduler.end] = contract;

#if INSTALL_GARBAGE
	executor->scheduler.allocated[executor->scheduler.end] = allocated;
#endif

	if (executor->scheduler.end < executor->scheduler.max)
		executor->scheduler.end += 1;
	else executor->scheduler.end = 0;

	if ((executor->scheduler.end + 1) % executor->scheduler.max != executor->scheduler.begin % executor->scheduler.max) {
#if DEBUG
		printf("requireTask: unlock updating\n");
#endif
		pthread_mutex_unlock(&executor->updating);
	}
	if (executor->scheduler.begin + 1 == executor->scheduler.end) {
#if DEBUG
		printf("requireTask: unlock waiting\n");
#endif
		pthread_mutex_unlock(&executor->waiting);
	}
}