#ifndef APPLICATION_H
#define APPLICATION_H

#include "udpev.h"

#if __cplusplus
extern "C" {
#endif
	typedef struct Application {
		struct {
			Receiver* ptr;
			int count;
		} receivers;
		Controller* controller;
		int count;
	} Application;

	Message parsing(Receiver* recver, char* data, int size);
	int     processing(Executor* execer, Contract* contract);
	int     install(Controller* controller, int id, int leveldb, const char* file, in_addr_t* addr);
	int 	observe(Application app);
#if __cplusplus
}
#endif
#endif