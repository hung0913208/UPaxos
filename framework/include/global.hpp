#if !defined(GLOBAL_HPP) && __cplusplus
#define GLOBAL_HPP

#include "framework.hpp"
#include "udpev.h"

/* Một macro dùng để định nghĩa quá trình config cho một đối tượng toàn cục của learner.
   Đối tượng này lưu tất cả các thông tin cấu hình cần thiết cho learner
 */
#define SConfig(content, index, body, install) static upaxos::Configure<content, content> config{index, body, install}
#define WConfig(content, application, index, body, install) static upaxos::Configure<content, application> config{index, body, install}

#define SConfigW(content, index, body, install, owner, finish) static upaxos::Configure<content, content> config{index, body, install, owner, finish}
#define WConfigW(content, application, index, body, install, owner, finish) static upaxos::Configure<content, application> config{index, body, install, owner, finish}

using Director = std::function<std::size_t(std::fstream&, std::vector<std::string>&)>;
template <typename Content>
using Action = std::vector<std::function<upaxos::Error(Content&, std::vector<std::string>&)>>;
template <typename Content>
using Install = std::function<void(Content&, upaxos::Variable* var)>;
template <typename Content>
using Owner = std::function<in_addr_t(Content&)>;
template <typename Content, typename Process>
using Done = std::function<void(upaxos::Configure<Content, Process>&)>;

/* Một macro dùng để định nghĩa quá trình parse packet ra các đối tượng message. Đối tượng
   này dùng để gán 
 */
#define Parse(message, unpack) static upaxos::ParseImpl<message> parser{unpack}
template <typename Msg>
using Unpack = std::function<int(char*, int, Msg&)>;
#endif