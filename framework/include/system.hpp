#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#if __cplusplus
#include <iostream>
#include <string>
#include <cstring>

#define __ERROR_LOCATE__ upaxos::Error::position(__FILE__, __LINE__, __PRETTY_FUNCTION__)
#define PError(code, message) upaxos::Error{__ERROR_LOCATE__, code, message}
namespace upaxos {
namespace error {
enum Code {
	NoError 	 = 0,
	KeepContinue = 1,
	NoSupport 	 = 2,
	BadLogic 	 = 3,
	OutOfRange 	 = 4,
	NotFound	 = 5
};
}
}

/* một số error cơ bản */
#define NoError      upaxos::Error{__ERROR_LOCATE__, upaxos::error::NoError}
#define KeepContinue upaxos::Error{__ERROR_LOCATE__, upaxos::error::KeepContinue}
#define NoSupport    upaxos::Error{__ERROR_LOCATE__, upaxos::error::NoSupport}
#define BadLogic     upaxos::Error{__ERROR_LOCATE__, upaxos::error::BadLogic}
#define OutOfRange	 upaxos::Error{__ERROR_LOCATE__, upaxos::error::OutOfRange}
#define NotFound	 upaxos::Error{__ERROR_LOCATE__, upaxos::error::NotFound}

namespace upaxos {
struct Error {
	std::string message, debug;
	int 		code;

	Error(std::string debug, int code = 0, std::string message = "");
	virtual ~Error();

	virtual std::string codename();

	operator bool();
	operator int();

	static std::string position(const char* file, int line, const char* proc);
};
}

std::ostream& operator <<(std::ostream& stream, upaxos::Error error);
#endif
#endif