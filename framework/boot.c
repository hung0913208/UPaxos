#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "application.h"

void usage(const char* application) {
  printf(
      "usage %s [-h | --help] [-c | --cache <n>] [--leveldb <flag>]"
      "[-p | --port <port>] --id <id> --recv <n> --var <n> <config.cfg>\n",
      application);
  printf(
      "\n\t%s:  %s\n\t%s:  %s\n\t%s:  %s\n\t%s:  %s\n\t%s:  %s\n\t%s:  "
      "%s\n\t%s:  %s\n\n",
      "--help", "print usage and exit", "--port",
      "port for receiving packet, its default value is 34959", "--id",
      "the id of this application", "--recv",
      "how many receivers have been build during installation", "--cache",
      "how many packets have been save in runner cache, its default value is "
      "10",
      "--leveldb", "enable using leveldb library, default is false", "--var",
      "define all variables have been used");
  printf("Example:\n\t%s --var 3 --recv 3 --id 0 config.cfg\n", application);
}

void quit(int error, const char* message) {
  printf("%s\n", message);
  exit(error);
}

Application construct(int argc, const char* argv[], Runner runner) {
  Application result = {{NULL, 0}, NULL, 0};

  int port = 34959, cache = 10, id = -1, leveldb = 0, i, j;
  int multiprocess = 0;
  Runner* runners = NULL;

  for (i = 1; i < argc; i += 2) {
    if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
      usage(argv[0]);
      exit(0);
    } else if (!strcmp(argv[i], "--multiprocess"))
      multiprocess = atoi(argv[i + 1]);
    else if (!strcmp(argv[i], "--id"))
      id = atoi(argv[i + 1]);
    else if (!strcmp(argv[i], "--leveldb"))
      leveldb = atoi(argv[i + 1]) != 0;
    else if (!strcmp(argv[i], "--port") || !strcmp(argv[i], "-p"))
      port = atoi(argv[i + 1]);
    else if (!strcmp(argv[i], "--cache") || !strcmp(argv[i], "-c")) {
      cache = atoi(argv[i + 1]);
    } else if (!strcmp(argv[i], "--recv")) {
      result.receivers.count = atoi(argv[i + 1]);
    } else if (!strcmp(argv[i], "--var")) {
      result.count = atoi(argv[i + 1]);

      if (runners) free(runners);
      if (!(runners = (Runner*)malloc(result.count * sizeof(Runner))))
        quit(-1, "Memory corruption");
      else
        for (j = 0; j < result.count; ++j) runners[j] = runner;
    } else
      break;
  }

  if (runners) {
    in_addr_t addr;

    if (!result.receivers.count) {
      free(runners);
      quit(-1, "Missing number of receivers");
    } else if (argc == i) {
      free(runners);
      quit(-1, "Missing file config");
    }

    result.receivers.ptr =
        (Receiver*)malloc(result.receivers.count * sizeof(Receiver));
    result.controller = installExecutors(runners, result.count, parsing, cache);

    if (install(result.controller, id, leveldb, argv[i], &addr))
      exit(-1);
    else if (installReceivers(result.receivers.ptr, result.receivers.count,
                              addr, port, result.controller)) {
      if (result.receivers.ptr) free(result.receivers.ptr);
      free(runners);
      quit(-1, "Memory corruption");
    }
    free(runners);
  } else
    quit(-1, "Memory corruption");
  return result;
}

int main(int argc, char const* argv[]) {
  return observe(construct(argc, argv, processing));
}
