#include "playground.hpp"
#include "application.h"

#include <iostream>
#include <cassert>
#include <memory>

int install(Controller* controller, int id, int leveldb, const char* file, in_addr_t* addr) {
	auto error = config.open(file);

	if (error) return error;
	else if (id < 0) return BadLogic;

	try {
		config.iddentity = id;
		config.leveldb = leveldb;

		for (auto i = 0; i < controller->count; ++i) {
			controller->executors[i].content =
			    upaxos::variable::instance(controller, i, config, parser);
		}
		*addr = config.multicast ? config.owner() : INADDR_ANY;
	} catch (upaxos::Error& error) { return error; }
	return 0;
}

Message parsing(Receiver* recv, char* data, int size) {
	Message result{};

	result.contract.size = size;
	if ((result.id = parser.parse(data, size, result.contract.msg)) < 0)
		return Message{};
	else return result;
}

int processing(Executor* exec, Contract* contract) {
	upaxos::Variable* generic{static_cast<upaxos::Variable*>(exec->content)};
	upaxos::Error error{__ERROR_LOCATE__};

	try {
		if (typeid(contract->msg) != parser.type()) {
#if INSTALL_GARBAGE
			auto result  = parser.convert(static_cast<Protocol>(contract->msg));
#else
			auto result  = parser.convert(static_cast<Protocol>(&contract->msg));
#endif
			auto content = upaxos::variable::cast(generic, config, result);

			if (content)
				error = content->_deliver(contract->sockid, config, exec, result, sizeof(result));

		} else {
			auto content = upaxos::variable::cast(generic, config, contract->msg);

			if (content)
				error = content->_deliver(contract->sockid, config, exec, contract->msg, contract->size);
		}
	} catch (upaxos::Error& error) {
#if !DEBUG
		std::cout << error << std::endl;
#endif
	}
	return 1;
}

int observe(Application app) {
	auto a = config;
	startService(app.receivers.ptr, app.receivers.count, app.controller, app.count);
	return 0;
}

namespace upaxos {
Variable::Variable() {}

Variable::~Variable() {}

}