#ifndef CONFIG_H
#define CONFIG_H
#define DEBUG 1

#if defined(PLAYGROUND_HPP) && __cplusplus
#include <vector>
#include <string>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "message.hpp"
#include "learner.hpp"

#include "leveldb_context.h"

/* - một ví dụ mẫu về contant dùng để thiết lập các tham số toàn cục cho learner của upaxos
   - có thể đặt ở bất kì đâu trong 2 file playground.hpp hoặc definition.hpp
   - nên ưu tiên đặt trong file definition.hpp
 */
#define Process TLearner
#define Content Caans
#define Machine paxos::Learner<MCmdMsg>

bool compare(MCmdMsg& a, MCmdMsg& b);

struct Caans {
	struct Address {
	private:
		std::string _address;
		int 	    _port;

	public:
		Address(): _port{ -1} {}

		Address(std::string& address, int port) {
			_address = address;
			_port 	  = port;
		}

	public:
		in_addr_t address() { return (_address.size()) ? inet_addr(_address.c_str()) : INADDR_ANY; }

		int port() { return _port; }
	};
	using Acceptor = std::vector<Address>;
	using Owner    = Address;

	Acceptor acceptors;
	Owner    owner;
	int 	 cacceptors, track;
	bool 	 verbose, multicast;

	Caans(): cacceptors{0}, verbose{false}, multicast{true} {}
};


struct TLearner {
	Machine machine;

	leveldb_ctx* leveldb;

	TLearner(): machine{compare} { leveldb = new_leveldb_context(); }
	~TLearner() { free_leveldb_context(leveldb); }
};


#endif

#define Protocol 		char*
#define INSTALL_GARBAGE 1
#endif