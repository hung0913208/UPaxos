#if !defined(PLAYGROUND_HPP) && __cplusplus
#define PLAYGROUND_HPP

/* - Một example nhỏ về cách sử dụng framework này để dựng một leaner cho upaxos lấy cảm hứng từ node.js và ruby
 * và áp dụng kiểu framework theo dạng source code giống như cách mà boost thường xuất bản thư viện của họ
 * - Lưu ý đây là 1 dạng framework kiểu source code build lại từ ban đầu nên tên playground.hpp phải cố định và
 * luôn tồn tại ngay trong thư mục configure
 */

/* ATTENTION, MUST BE NOTICE:
 * phải luôn load framework để có thể sử dụng framework
 */

#include "message.hpp"
#include "global.hpp"

/* - file definition.hpp chứa  các định nghĩa cho cả playground.hpp lẫn udpev
 * - có thể không cần include nó vẫn được
 */

/* - file playground.hpp chứa tất cả các mô tả để cấu hình upaxos chạy từ hướng mô tả cách để load file config.cfg
 * - thông tin thu được từ file config.cfg sau khi trích lược
 */

/* token và transaction là 2 lambda có nhiệm vụ chỉ hướng cho configure phân tích cấu hình
 * 	- token dùng để đọc một chuỗi và xác định xem phải chạy transaction nào
 * 	- transactions là 1 vector các transaction lambda theo thứ tự, khi token trả về gía trị số, tương ứng
 * transaction thứ mấy sẽ thực hiện việc xử lý dựa trên các thẻ từ mà token sinh ra
 *  - sau khi hoàn thành quá trình đọc cấu hình, chương trình sẽ tự động gọi install để cài đặt cho từng
 *  thread thực thi
 */

/* owner là lambda có trách nhiệm trả về địa chỉ ip của chính máy chủ, nếu không đăng kí thì mặc định là
 * ẩn và không dùng multicast ip được
 */

/* ghi chú nhỏ về lambda là một kiểu anonymous function. Thay vì ta định nghĩa tường minh một hàm, c++ hỗ trợ
 * việc định nghĩa hàm ngắn gọn hơn theo kiểu như 1 biến và lưu lại dễ dàng. Cách sử dụng không khác con trỏ
 * hàm
 */

/* các hàm giao tiếp để kết nối thread với hàm thực thi việc xử lý */

bool load(std::fstream& cfg, std::vector<std::string>& output, std::size_t count);
void print_addr(struct sockaddr_in* s);
int deserilization(char* ptr, int size, Protocol& msg);
upaxos::Error handle(int sockid, upaxos::Configure<Caans,TLearner>& config, Executor* exec, char*& msg, int size);

/* - namespace configure chứ code cấu hình chuẩn cho caans, nó đúng cho tất cả các thành phần miễn là dùng chung
   kiểu định dạng của caans, file cấu hình chuẩn caans được đưa ra sẵn(vốn dĩ là bản cấu hình cũ của libpaxos)
   - không cần phải viết lại namespace này trừ phi có nhu cầu dùng cấu hình khác
 */
namespace configure {
enum ActionIndex {
	NumAcceptors = 0,
	Verbosity = 1,
	Acceptor = 2,
	Learner = 3,
	Tracking = 4,
	Multicast = 5,
	PreExecWindow = 6,
	Coordinator = 7,
	ProxyPort = 8,
	Proposer = 9
};

static Director token = [](std::fstream& cfg, std::vector<std::string>& output) -> std::size_t {
	std::string action;

	cfg >> action;
	output.clear();

	/* bảng phân loại token dựa theo token đầu tiên để xác định */
	if (action == "num_acceptors")
		return load(cfg, output, 1) ? NumAcceptors : std::string::npos;
	else if (action == "verbosity")
		return load(cfg, output, 1) ? Verbosity : std::string::npos;
	else if (action == "acceptor")
		return load(cfg, output, 2) ? Acceptor : std::string::npos;
	else if (action == "learner")
		return load(cfg, output, 2) ? Learner : std::string::npos;
	else if (action == "tracking")
		return load(cfg, output, 1) ? Tracking : std::string::npos;
	else if (action == "multicast")
		return load(cfg, output, 1) ? Multicast : std::string::npos;
	else if (action == "proposer_preexec_window")
		return load(cfg, output, 1) ? PreExecWindow : std::string::npos;	
	else if (action == "coordinator")
		return load(cfg, output, 2) ? Coordinator : std::string::npos;
	else if (action == "proxy_port")
		return load(cfg, output, 1) ? ProxyPort : std::string::npos;
	else if (action == "proposer")
		return load(cfg, output, 2) ? Proposer : std::string::npos;
	else return std::string::npos;
};
static Action<Content> transaction = {
	[](Content & content, std::vector<std::string>& tokens) -> upaxos::Error{
		/* token num_acceptors chứa số lượng acceptor của chế độ multicast */
		content.cacceptors = std::stoi(tokens[0]);
		return NoError;
	},
	[](Content & content, std::vector<std::string>& tokens) -> upaxos::Error{
		/* token verbosity chứa 1 flag để xác định learner ở chế độ debug hay không */
		content.verbose = tokens[0] == "debug";
		return (tokens[0] != "quiet" && !content.verbose) ? NoSupport : NoError;
	},
	[](Content & content, std::vector<std::string>& tokens) -> upaxos::Error{
		/* - token acceptor chứa thông tin ip của acceptor
		 * - acceptor được lưu nhiều hơn 1 thì có nghĩa là acceptor là một SingleLatch,
		 * ngược lại nếu cacceptors > 0 có nghĩa là acceptors chỉ có thể chứ 1 đia chỉ duy nhất
		 * và địa chỉ này là một Multicast
		 */
		if (content.cacceptors && content.acceptors.size())
			return BadLogic;
		content.acceptors.push_back(Content::Address{tokens[0], std::stoi(tokens[1])});
		return NoError;
	},
	[](Content & content, std::vector<std::string>& tokens) -> upaxos::Error{
		/* - token learner chứa thông tin ip của chính bản thân learner
		 * - trách nhiệm của token này là để định danh tính của learner trên mạng debug
		 */
		content.owner = Content::Address{tokens[0], std::stoi(tokens[1])};
		return NoError;
	}, [](Content & content, std::vector<std::string>& tokens) -> upaxos::Error{
		/* - token learner chứa thông tin ip của chính bản thân learner
		 * - trách nhiệm của token này là để định danh tính của learner trên mạng debug
		 */
		content.track = std::stoi(tokens[0]);
		return NoError;
	}, [](Content & content, std::vector<std::string>& tokens) -> upaxos::Error{
		/* - token learner chứa thông tin ip của chính bản thân learner
		 * - trách nhiệm của token này là để định danh tính của learner trên mạng debug
		 */
		content.multicast = tokens[0] == "true";
		return NoError;
	}, nullptr, nullptr, nullptr, nullptr
};
static Install<Content> installation = [](Content& content, upaxos::Variable* var){
	/* lưu ý, lambda này dùng để cấu hình cho từng thread thực thi(thread executor) */
	auto impl  = upaxos::variable::cast<Content, Process, Protocol>(var);
	auto count = content.cacceptors ? content.cacceptors : content.acceptors.size();

	/* tiến hành cài đặt learner của thư viện paxos cho từng thread */
	impl->content().machine = Machine{compare, count};
	impl->deliver(handle);
	return impl->content();
};
static Owner<Content> owner = [](Content& content) -> in_addr_t{
	return content.owner.address();
};
static Done<Content, Process> done = [](upaxos::Configure<Content, Process>& thiz){
	thiz.multicast = thiz.content().multicast && thiz.content().owner.port() > 0;
};
}

/* cuối cùng ta khai báo bản đồ xử lý này vào block Config
 * block SConfigW là một macro nhỏ giúp tạo ra biến configure toàn cục với kiểu Content chứa thông tin đã
 * được trích chọn từ file config.cfg cho hệ thống learner
 * chi tiết các macro có thể tham khảo trong file global.hpp
 */
WConfigW(Content, Process, configure::token, configure::transaction, configure::installation, configure::owner, configure::done);
/* ------------------------------------------------------------------------------------------------------------ */
/* định nghĩa quá trình phân gỉải paxos_message từ packet mà acceptor gửi đi vào các receiver */


/* vì giao thức đơn giản nên ta có thể giao trách nhiệm phân tích và xử lý cho thread xử lý */
static Unpack<Protocol> unpack = deserilization;

Parse(Protocol, unpack);

#define Cast(exec) upaxos::variable::cast<Content, Process, Protocol>(static_cast<upaxos::Variable*>(exec->content))
#endif
