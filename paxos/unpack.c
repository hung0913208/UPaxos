#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "message.hpp"

/* TODO: compute offsets */
#define MSGTYPE_OFFSET              0
#define IID_OFFSET                  2
#define BALLOT_OFFSET               6
#define THREAD_ID_OFFSET            8
#define ACCEPTOR_COUNTER_ID_OFFSET  10
#define VALUE_BALLOT_OFFSET         12
#define AID_OFFSET                  14
#define VALUE_OFFSET                16

/* paxos_repeat offsets */
#define FROM_OFFSET                         2
#define TO_OFFSET                           6
#define REPEAT_THREAD_ID_OFFSET             10
#define REPEAT_ACCEPTOR_COUNTER_ID_OFFSET   12

/* paxos_trim offset */
#define TRIM_OFFSET         2

/* paxos_acceptor_state offsets */
#define STATE_AID_OFFSET    2
#define TRIM_IID_OFFSET     6

MLearnerMsg getPrepareMsg(const char* p, int size) {
    MLearnerMsg result;

    assert(p[size] == '\0');

    result.iid       = getWord(p, IID_OFFSET);
    result.ballot    = getWord(p, BALLOT_OFFSET);
    result.thid      = getWord(p, THREAD_ID_OFFSET);
    result.atid      = getWord(p, ACCEPTOR_COUNTER_ID_OFFSET);
    result.bvalue    = getWord(p, VALUE_BALLOT_OFFSET);
    result.aid       = getWord(p, AID_OFFSET);
    result.cmd.size  = getDWord(p, VALUE_OFFSET);
    result.cmd.ptr   = getValue(p, VALUE_OFFSET);
    return result;
}

MLearnerMsg getPromiseMsg(const char* p, int size) {
    MLearnerMsg result;

    assert(p[size] == '\0');

    result.iid       = getDWord(p, IID_OFFSET);
    result.ballot    = getWord(p, BALLOT_OFFSET);
    result.thid      = getWord(p, THREAD_ID_OFFSET);
    result.atid      = getWord(p, ACCEPTOR_COUNTER_ID_OFFSET);
    result.bvalue    = getWord(p, VALUE_BALLOT_OFFSET);
    result.aid       = getWord(p, AID_OFFSET);
    result.cmd.size  = getDWord(p, VALUE_OFFSET);
    result.cmd.ptr   = getValue(p, VALUE_OFFSET);

    return result;
}

MLearnerMsg getAcceptMsg(const char* p, int size) {
    MLearnerMsg result;

    assert(p[size] == '\0');

    result.iid       = getDWord(p, IID_OFFSET);
    result.ballot    = getWord(p, BALLOT_OFFSET);
    result.thid      = getWord(p, THREAD_ID_OFFSET);
    result.atid      = getWord(p, ACCEPTOR_COUNTER_ID_OFFSET);
    result.bvalue    = getWord(p, VALUE_BALLOT_OFFSET);
    result.aid       = getWord(p, AID_OFFSET);
    result.cmd.size  = getDWord(p, VALUE_OFFSET);
    result.cmd.ptr   = getValue(p, VALUE_OFFSET);

    return result;
}

MLearnerMsg getAcceptedMsg(const char* p, int size) {
    MLearnerMsg result;

    assert(p[size] == '\0');

    result.iid       = getDWord(p, IID_OFFSET);
    result.ballot    = getWord(p, BALLOT_OFFSET);
    result.thid      = getWord(p, THREAD_ID_OFFSET);
    result.atid      = getWord(p, ACCEPTOR_COUNTER_ID_OFFSET);
    result.bvalue    = getWord(p, VALUE_BALLOT_OFFSET);
    result.aid       = getWord(p, AID_OFFSET);
    result.cmd.size  = getDWord(p, VALUE_OFFSET);
    result.cmd.ptr   = getValue(p, VALUE_OFFSET);

    return result;
}

MLearnerMsg getPreemptMsg(const char* p, int size) {
    MLearnerMsg result;

    assert(p[size] == '\0');

    result.iid       = getDWord(p, IID_OFFSET);
    result.ballot    = getWord(p, BALLOT_OFFSET);
    result.thid      = getWord(p, THREAD_ID_OFFSET);
    result.atid      = getWord(p, ACCEPTOR_COUNTER_ID_OFFSET);
    result.bvalue    = getWord(p, VALUE_BALLOT_OFFSET);
    result.aid       = getWord(p, AID_OFFSET);
    result.cmd.size  = getDWord(p, VALUE_OFFSET);
    result.cmd.ptr   = getValue(p, VALUE_OFFSET);

    return result;
}

ARepeatMsg getRepeatMsg(const char* p, int size) {
    ARepeatMsg result;

    assert(p[size] == '\0');

    result.from = getDWord(p, FROM_OFFSET);
    result.to   = getDWord(p, TO_OFFSET);
    result.thid = getWord(p, REPEAT_THREAD_ID_OFFSET);
    result.atid = getWord(p, REPEAT_ACCEPTOR_COUNTER_ID_OFFSET);

    return result;
}

AActStateMsg getAcceptorStateMsg(const char* p, int size) {
    AActStateMsg result;

    assert(p[size] == '\0');

    result.atid  = getDWord(p, STATE_AID_OFFSET);
    result.triid = getDWord(p, TRIM_IID_OFFSET);

    return result;
}

MCmdMsg getClientCmdMsg(const char* p, int size) {
    MCmdMsg result;

    assert(p[size] == '\0');

    result.size = getDWord(p, 0);
    result.ptr  = getValue(p, 0);

    return result;
}

int getThreadId(const char* p) {
    switch (getType(p)) {
    case PaxosPrepare:
    case PaxosPromise:
    case PaxosAccept:
    case PaxosAccepted:
    case PaxosPreempted:
        return getWord(p, THREAD_ID_OFFSET);

    case PaxosRepeat:
        return getWord(p, REPEAT_THREAD_ID_OFFSET);

    default:
        return -1;
    }
}