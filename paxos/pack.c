
#define PutDWord(p, offset, value) *((uint32_t*)(p + offset)) = htonl(value)

#define PutWord(p, offset, value) *((uint16_t*)(p + offset)) = htons(value)

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "message.hpp"

/* TODO: compute offsets */
#define MSGTYPE_OFFSET 0
#define IID_OFFSET 2
#define BALLOT_OFFSET 6
#define THREAD_ID_OFFSET 8
#define ACCEPTOR_COUNTER_ID_OFFSET 10
#define VALUE_BALLOT_OFFSET 12
#define AID_OFFSET 14
#define VALUE_OFFSET 16

/* paxos_repeat offsets */
#define FROM_OFFSET 2
#define TO_OFFSET 6
#define REPEAT_THREAD_ID_OFFSET 10
#define REPEAT_ACCEPTOR_COUNTER_ID_OFFSET 12

/* paxos_trim offset */
#define TRIM_OFFSET 2

/* paxos_acceptor_state offsets */
#define STATE_AID_OFFSET 2
#define TRIM_IID_OFFSET 6

int setPrepareMsg(MLearnerMsg* msg, MBufferWriter* buffer) {
  if (buffer->max < VALUE_OFFSET + msg->cmd.size + 4) return 0;
  putWord(buffer, msg->iid, IID_OFFSET);
  putWord(buffer, msg->ballot, BALLOT_OFFSET);
  putWord(buffer, msg->thid, THREAD_ID_OFFSET);
  putWord(buffer, msg->atid, ACCEPTOR_COUNTER_ID_OFFSET);
  putWord(buffer, msg->bvalue, VALUE_BALLOT_OFFSET);
  putWord(buffer, msg->aid, AID_OFFSET);
  putDWord(buffer, msg->cmd.size, VALUE_OFFSET);
  putValue(buffer, msg->cmd, VALUE_OFFSET);
  return 1;
}

int setMLearnerMsg(MLearnerMsg* msg, MBufferWriter* buffer) {
  if (buffer->max < VALUE_OFFSET + msg->cmd.size) return 0;
  putDWord(buffer, msg->iid, IID_OFFSET);
  putWord(buffer, msg->ballot, BALLOT_OFFSET);
  putWord(buffer, msg->thid, THREAD_ID_OFFSET);
  putWord(buffer, msg->atid, ACCEPTOR_COUNTER_ID_OFFSET);
  putWord(buffer, msg->bvalue, VALUE_BALLOT_OFFSET);
  putWord(buffer, msg->aid, AID_OFFSET);
  putDWord(buffer, msg->cmd.size, VALUE_OFFSET);
  putValue(buffer, msg->cmd, VALUE_OFFSET);
  return 1;
}

int setRepeatMsg(ARepeatMsg* msg, MBufferWriter* buffer) {
  if (buffer->max < REPEAT_ACCEPTOR_COUNTER_ID_OFFSET + sizeof(msg->atid)) return 0;
  putDWord(buffer, msg->from, FROM_OFFSET);
  putDWord(buffer, msg->to, TO_OFFSET);
  putWord(buffer, msg->thid, REPEAT_THREAD_ID_OFFSET);
  putWord(buffer, msg->atid, REPEAT_ACCEPTOR_COUNTER_ID_OFFSET);
  return 1;
}

int setAcceptorStateMsg(AActStateMsg* msg, MBufferWriter* buffer) {
  if (buffer->max < TRIM_IID_OFFSET + sizeof(msg->triid)) return 0;
  putDWord(buffer, msg->atid, STATE_AID_OFFSET);
  putDWord(buffer, msg->triid, TRIM_IID_OFFSET);
  return 1;
}

int setClientCmdMsg(MCmdMsg* msg, MBufferWriter* buffer) {
  if (buffer->max < msg->size + 4) return 0;
  putDWord(buffer, msg->size, 0);
  putValue(buffer, msg[0], 0);
  return 1;
}

int setThreadId(const char* p) {
  switch (getType(p)) {
    case PaxosPrepare:
    case PaxosPromise:
    case PaxosAccept:
    case PaxosAccepted:
    case PaxosPreempted:
      return getWord(p, THREAD_ID_OFFSET);

    case PaxosRepeat:
      return getWord(p, REPEAT_THREAD_ID_OFFSET);

    default:
      return -1;
  }
}