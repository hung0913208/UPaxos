#if !defined(LEARNER_HPP) && __cplusplus
#define LEARNER_HPP

#include "system.hpp"

#include <vector>
#include <functional>
#include <map>

namespace paxos {
using IId      = std::size_t;

template<typename Message>
struct Learner {
private:
	using AId      = std::size_t;
	using Ballot   = std::size_t;
	using CAcks	   = std::size_t;

public:
	using Acks 	  = std::vector<Ballot>;
	using Store   = std::map<IId, std::tuple<Message, Ballot, Acks>>;
	using Key 	  = std::tuple<IId, AId>;
	using Compare = std::function<bool(Message&, Message&)>;

	Learner(Compare compare, CAcks cacceptor = 0, bool custom = false): _cacceptor{cacceptor} {
		_current = !custom;
		_compare = compare;
	}

	~Learner() {}

public:
	IId deliver(Message &msg) {
		if (existed(_current) && quorum(_current)) {
			msg = detach(_current);
			return _current++;
		}
		return std::string::npos;
	}

	upaxos::Error load(Key key, Ballot ballot, Message value) {
		if (!_current) _current = iid(key);
		else if (iid(key) < _current)
			return BadLogic;

		if (!existed(key))
			_store[iid(key)] = make(value, ballot);
		else if (!_compare(get(key), value))
			return BadLogic;
		else if (quorum(key))
			return OutOfRange;
		return update(key, ballot);
	}

public:
	IId running(){ return _current; }

	IId hiwait(){ return _hiwait; }

	int track(){ return 0; }
	
private:
	bool existed(IId iid) {
		return _store.find(iid) != _store.end();
	}

	bool existed(Key& key) {
		return _store.find(std::get<0>(key)) != _store.end();
	}

	bool quorum(Key& key) { return quorum(std::get<0>(key)); }

	bool quorum(IId iid) {
		auto newest = ballot(iid), count = std::size_t{0};

		if (ballot(iid) == std::string::npos)
			return true;

		for (auto ack : acks(iid)) {
			if (ack != std::string::npos)
				count += (ack == newest);
		}
		return count >= ((_cacceptor / 2) + 1);
	}

private:
	Message& get(Key& key) { return std::get<0>(_store[std::get<0>(key)]); }

	IId iid(Key& key) { return std::get<0>(key); }

	AId aid(Key& key) { return std::get<1>(key); }

	Ballot ballot(IId iid) { return std::get<1>(_store[iid]); }

	Acks acks(IId iid) { return std::get<2>(_store[iid]); }

	Message& msg(IId iid) { return std::get<0>(_store[iid]); }

private:
	std::tuple<Message, Ballot, Acks> make(Message& value, Ballot ballot) {
		return std::make_tuple(value, ballot, std::vector<Ballot> (_cacceptor, Ballot{std::string::npos}));
	}

	upaxos::Error update(Key& key, Ballot ballot) {
		auto id   = iid(key);
		auto ack  = std::get<2>(_store[id])[aid(key)];

		if (ack != std::string::npos && ack > ballot)
			return BadLogic;

		std::get<1>(_store[id]) 		  = ballot;
		std::get<2>(_store[id])[aid(key)] = ballot;
		return NoError;
	}

	Message detach(IId iid) {
		Message result = msg(iid);

		_store.erase(iid);
		return result;
	}

private:
	IId	    _current, _hiwait;
	Compare _compare;
	CAcks   _cacceptor;	
	Store   _store;
};
}
#endif
