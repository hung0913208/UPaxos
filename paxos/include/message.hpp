#ifndef MESSAGE_H
#define MESSAGE_H

#include <arpa/inet.h>
#include <stdint.h>

#define Word uint16_t
#define DWord uint32_t
#define Bytes char*

#if __cplusplus
extern "C" {
#endif

enum PaxosType {
  PaxosPrepare,
  PaxosPromise,
  PaxosAccept,
  PaxosAccepted,
  PaxosPreempted,
  PaxosRepeat,
  PaxosTrim,
  PaxosAcceptState,
  PaxosClientValue
};

typedef struct MCmdMsg {
  DWord size;
  Bytes ptr;
} MCmdMsg;

typedef struct MLearnerMsg {
  DWord iid;
  Word ballot, thid;
  Word atid, aid, bvalue;
  MCmdMsg cmd;
} MLearnerMsg;

typedef struct ARepeatMsg {
  DWord from, to;
  Word thid, atid;
} ARepeatMsg;

typedef struct MBufferWriter {
  char* data;
  int size, max;
} MBufferWriter;

#define TrimMsg DWord

typedef struct AActStateMsg { Word triid, atid; } AActStateMsg;

#define TRIM_OFFSET 2

#define getDWord(p, offset) ntohl(*((uint32_t*)((char*)p + offset)))
#define getWord(p, offset) ntohs(*((uint16_t*)((char*)p + offset)))
#define getLoDWord(p, offset) *((uint32_t*)((char*)p + offset))
#define getLoWord(p, offset) *((uint16_t*)((char*)p + offset))
#define getValue(p, offset) (char*)((char*)p + offset + 4)
#define getObject(p, offset, object) *((object*)((char*)p + offset))
#define getRaw(p, offset) (char*)((char*)p + offset)

#define getTrimMsg(p) getDWord(p, TRIM_OFFSET)
#define getType(p) getWord(p, 0)

inline const void* __fix__(void* data, int size) {
  if (*((uint16_t*)"\0\xff") < 0x100) {
    for (int i = 0; i < size / 2; ++i)
      ((char*)data)[size - i] = ((char*)data)[i];
  }
  return data;
}

#define putDWord(b, p, offset) memcpy(b->data + offset, __fix__((void*)&(p), sizeof(uint32_t)), sizeof(uint32_t))
#define putWord(b, p, offset) memcpy(b->data + offset, __fix__(&(p), sizeof(uint32_t)), sizeof(uint16_t))
#define putLoDWord(b, p, offset) memcpy(b->data + offset, &(p), sizeof(uint32_t))
#define putLoWord(b, p, offset) memcpy(b->data + offset, &(p), sizeof(uint16_t))
#define putValue(b, p, offset) memcpy(b->data + offset + 4, p.ptr, p.size)

MLearnerMsg getPrepareMsg(const char* p, int size);
MLearnerMsg getPromiseMsg(const char* p, int size);
MLearnerMsg getAcceptMsg(const char* p, int size);
MLearnerMsg getAcceptedMsg(const char* p, int size);
MLearnerMsg getPreemptMsg(const char* p, int size);
ARepeatMsg getRepeatMsg(const char* p, int size);
AActStateMsg getAcceptorStateMsg(const char* p, int size);
MCmdMsg getClientCmdMsg(const char* p, int size);
int getThreadId(const char* p);

int setPrepareMsg(MLearnerMsg* msg, MBufferWriter* buffer);
int setMLearnerMsg(MLearnerMsg* msg, MBufferWriter* buffer);
#define setAcceptMsg setMLearnerMsg
#define setAcceptedMsg setMLearnerMsg
#define setPreemptMsg setMLearnerMsg

#if __cplusplus
}
#endif
#endif